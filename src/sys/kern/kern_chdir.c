#include "syscall.h"
/*
    chdir syscall
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
void chdir(char *path)
{
    asm volatile
    (
        "mov $6, %%eax\n\t"
        "mov %0, %%ebx\n\t"
        "int $0x80\n\t"
        "mov %%eax, %1"
        : "=r" (path)
        : "r" (path)
        : "%eax", "%ebx"
    );
}
