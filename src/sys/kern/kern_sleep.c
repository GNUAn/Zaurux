#include "syscall.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
    sleep syscall
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

void sleep(unsigned sec)
{
    time_t t = time(NULL);
    time_t t2 = time(NULL);
    for (;;)
    {
        t2 = time(NULL);
        if (t2 - t > 0)
          {
            if (t2 - t >= sec)
              {
                break;
                return 0;
            	}
              else 
                {
                  // Do nothing ( ͡° ͜ʖ ͡°) 
                }
            }
        else if (time(NULL) < t)
            {
              // Do nothing \_(ツ)_/
            }
			return; 
    }
}