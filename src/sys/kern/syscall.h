#include "sys/types.h"
/*
    syscall definitions
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
void sleep(unsigned);
void idle(void);
pid_t fork(void);
void panic(char *s);
void idle(void);
int execl(const char *, const char*, ...);
int execv(const char *, char * const*);
int execvp(const char *, char * const*);
int execvl(const char *, const char **, const char **);
int execve(const char *, char * const*, char * const*);
pid_t waitpid(pid_t pid, int *status, int options);
void chdir(char *path);
int mkdir(const char *path, mode_t mode);