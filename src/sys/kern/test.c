#include "syscall.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/*
    test program for syscalls
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
int main(int argc, char **argv)
{
    sleep(1);
    pid_t pid = fork();
    if (pid == 0)
    {
        printf("Hello, world!\nI am a child process!\n");
        if (argc > 1)
        {
            printf("I want to sleep, see you in %d seconds!\n", atoi(argv[1]));
            sleep(atoi(argv[1]));
        }
        else
        {
            srand(time(NULL));
            int random = rand()%10+1;
            printf("I want to sleep, see you in %d seconds!\n", random);
            sleep(random);
        }
        printf("I want to go to your home directory, please, type in path for it\n");
        char **path;
        fgets(path[0], 511, stdin);
        chdir(path[0]);
        system("ls -l");
    }
    else if (pid == -1)
    {
        printf("Oops, i wasn't forked properly.\n");
        panic("fork failed");
    }
    else
    {
        printf("I am a parent process, i'm waiting for my child to go back\n");
        wait(NULL);
    }
    return (0);
}
