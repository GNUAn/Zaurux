#ifndef _SYS_TYPES_H
#define _SYS_TYPES_H
/*
    types.h
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef _SIZE_T
#define _SIZE_T
typedef unsigned int size_t;
#endif

#ifndef _OFF_T
#define _OFF_T
typedef long int off_t;
#endif

#ifndef _PID_T
#define _PID_T
typedef int pid_t;
#endif

#ifndef _UID_T
#define _UID_T
typedef unsigned int uid_t;
#endif

#ifndef _GID_T
#define _GID_T
typedef unsigned int gid_t;
#endif

#ifdef __cplusplus
}
#endif

#endif