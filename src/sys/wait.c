#include "wait.h"
/*
    wait syscall
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
int waitpid(pid_t pid, int *_Nullable wstatus, int options)
{
    asm volatile
    (
        "mov $5, %%eax\n\t"
        "mov %0, %%ebx\n\t"
        "mov %1, %%ecx\n\t"
        "mov %2, %%edx\n\t"
        "int $0x80\n\t"
        "mov %%eax, %3"
        : "=r" (*wstatus)
        : "r" (pid), "r" (options), "r" (status)
        : "%eax", "%ebx", "%ecx", "%edx"
    );
    return 0;
}
int wait(int *_Nullable wstatus)
{
  waitpid(-1, wstatus, 0);
  return 0;
}
