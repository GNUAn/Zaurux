#include <stdio.h>
/*
    calculator
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
long long
exponent(int a, int b)
{
      if (b == 0)
      {
            return (1);
      }
      long long c = a;
      for (int i = 1; i < b; i++)
      {
            c = c*a;
      }
      return (c);
}
/* Calculator for Zaurux operating system */
int 
main(void)
{
  long long a, b = 0;
  short int c = 0;
  puts("Enter two numbers:\t");
  scanf("%lld %lld", &a, &b);
  puts("Enter operation symbol:\t");
  getchar();
  c = getchar();
  switch(c)
    {
      case '+':
	      printf("%lld+%lld=%lld", a, b, a+b);
	      break;
      case '-':
	      printf("%lld-%lld=%lld", a, b, a-b);
	      break;
      case '*':
	      printf("%lld*%lld=%lld", a, b, a*b);
	      break;
      case '/':
	      printf("%lld/%lld=%lld", a, b, a/b);
	      break;
      case '^':
            printf("%lld^%lld=%lld", a, b, exponent(a, b));
            break;
      default:
	      printf("%c - invalid symbol\n", c);
	      break;
    }
  putchar('\n');
  return (0);
}
