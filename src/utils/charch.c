#include <stdio.h>
#include <string.h>
/*
    char change
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
int main(int argc, char **argv)
{
  for (int i = 0; i < strlen(argv[1]); i++)
    {
      if (argv[1][i] == argv[2][0])
        argv[1][i] = argv[3][0];
    }
  puts(argv[1]);
  return (0);
}
