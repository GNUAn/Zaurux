#include <stdio.h>
/*
    echo
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
int main(int argc, char **argv)
{
  if (argc < 3)
    printf("%s\n", argv[1]);
  else if (argv[2][0] == '>')
    {
      FILE *f = fopen(argv[3], "w");
      fprintf(f, argv[1]);
      fclose(f);
    }
  else
    {
      printf("You forgot to put '>' before the file name.\n");
      return -1;
    }
  return 0;
}