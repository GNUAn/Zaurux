#include "../max.h"
/*
    memory game
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
const char *words[] = {"chocolate\0", "apple\0", "theater\0", "computer\0", "orange\0", "banana\0", "mouse\0", "piano\0", "house\0", "zaurux\0"};

int 
main(int argc, char **argv)
{
    system("clear");
    char buf[1024];
    srand(time(NULL));
    for (;;)
    {
        system("clear");
        int i = rand()%9;
        printf("%s\n", words[i]);
        sleep(rand()%10+5);
        system("clear");
        printf("What word just dissapeared?\n");
        fgets(buf, 1023, stdin);
        buf[strlen(buf)-1] = '\0';
        if (strcmp(buf, words[i]) == 0)
            {
                printf("Yes! It was %s\nTo continue, press Enter, to end the game, press CTRL+C\n", words[i]);
                getchar();
            }
        else
            {
                printf("No! It was %s\nTo continue, press Enter, to end the game, press CTRL+C\n", words[i]);
                getchar();
            }
    }
}
