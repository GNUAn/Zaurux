#include <stdio.h>
/*
    HQ9+ programming language implementation
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
int k = 0;
void beer99(void)
{
	for (int beer = 99; beer > 1; --beer)
	{
		printf("%d bottles of beer on the wall, %d bottles of beer\n", beer, beer);
		printf("Take one down, pass it around, %d bottles of beer on the wall!\n", --beer);
	}
	puts("1 bottle of beer on the wall, 1 bottle of beer\nTake on down, pass it around, no more bottles of beer on the wall!\n");
	puts("No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some, 99 bottles of beer on the wall\n");
}
int hq9p(char *source)
{
  for (int i = 0; i < sizeof(source); i++)
  {
	switch(source[i])
	  	{
		  	case 'H':
				printf("Hello, world!\n");
				break;
		  	case 'Q':
				printf("%s\n", source);
				break;
		  	case '9':
				beer99();
				break;
		  	case '+':
		  		++k;
			case 'P':
		  		++k;
		  	break;
	  	}
  }
  return (0);
}
void hq9open(char *name)
{
  FILE *f = fopen(name, "r");
  char source[1024];
  fgets(source, 1023, f);
  hq9p(source);
}
int main(int argc, char **argv)
{
  hq9open(argv[1]);
}