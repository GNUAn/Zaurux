#include <dirent.h>
#include <stdio.h>
#include <unistd.h>

/*
    ls
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

int ls(char*, int, int);
int ls(path, a, b)
char *path;
int a;
int b;
{
  struct dirent *d;
  DIR *dh = opendir(path);
  if (!dh)
    {
      perror("Directory doesn't exist\n");
    }
  else
    {
      while ((d = readdir(dh)) != NULL)
        {
                if (!a && d->d_name[0] == '.')
                        continue;
                  printf("%s  ", d->d_name);
                if(b) printf("\n");
        }
    }
}
main(argc, argv)
int argc;
char **argv;
{
  if (argv[0][0] == '\0')
    argv[0][0] = '.';
  ls(argv[1], 0, 0);
}