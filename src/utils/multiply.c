#include <stdio.h>
#include <string.h>
/*
    multiplying table
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
int
main(int argc, char **argv)
{
    if (fopen(argv[1], "r") == NULL && argc > 1)
        {
            FILE *bin = fopen(argv[1], "w");
            fclose(bin);
        }
    FILE *f;
    if (argc > 1)
        f = fopen(argv[1], "w");
    else if (argc < 2)
        {
            f = fopen("/dev/null", "w");
        }
    for (int i = 0; i < 1024; i++)
    {
        for (int j = 0; j < 1024; j++)
        {
            fprintf(f, "\0");
        }
    }
    for (int i = 1; i < 10; i++)
        {
            for (int j = 1; j < 10; j++)
                {
                    if (argc > 1)
                        fprintf(f, "%d\t", i * j);
                    else
                        fprintf(stdout, "%d\t", i * j);
                }
            fprintf(f, "\n");
        }
    fclose(f);
    return (0);
}