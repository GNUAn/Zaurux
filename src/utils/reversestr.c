/*
    echo
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <string.h>
void str_reverse(char *str);
void str_reverse(char *str)
{
    if (str == 0)
        {
            return;
        }

    if (*str == 0)
        {
            return;
        }

    char *start = str;
    char *end = start + strlen(str) - 1;
    char temp;

    while (end > start)
    {
        temp = *start;
        *start = *end;
        *end = temp;

        ++start;
        --end;
    }
}
int main(int argc, char **argv)
{
    char str[512];
    strcpy(str, argv[1]);
    str_reverse(str);
    puts(str);
    return 0;
}