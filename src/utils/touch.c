#include <stdio.h>

/*
    touch
    Copyright (C) 2024  GNUAn

    This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

int main(int argc, char **argv)
{
    FILE *f;
    for (int i = 2; i < argc; i++) 
    {
        if (fopen(argv[i-1], "r") != NULL)
            {
                printf("File %s already exists\n", argv[i-1]);
                return -1;
            }
        f = fopen(argv[i-1], "w");
        fclose(f);
    }
}